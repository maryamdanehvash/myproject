import { getUsers } from "./baseUrl";

const getAllUsers = async () => {
  const { data } = await getUsers({
    method: "GET",
    url: "/users",
  });
  return data;
};

export {getAllUsers}
