import { Typography } from "@mui/material";
import { Box } from "@mui/system";
import Wrapper from "../components/Wrapper";
import { useAllUsers } from "../hooks";
import { useStyles } from "./styles";

export const Users = () => {
  const classes = useStyles();
  const { data: users, isLoading } = useAllUsers();
  let usersMarkup;
  if (isLoading) return <Typography>loading ...</Typography>
  if (!isLoading && users) {
    console.log(users);
    usersMarkup = users.map((user: any) => {
      return <Wrapper>{user.name}</Wrapper>;
    });
  }
  return <Box className={classes.bgColor}>{usersMarkup}</Box>
};
