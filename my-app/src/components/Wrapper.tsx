import Card from "@mui/material/Card";
import Container from "@mui/material/Container";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import React from "react";
import Box from "@mui/material/Box";
import { useStyles } from "./styles";

interface Props {
  children?: React.ReactNode;
}
export function Wrapper(props: Props) {
  const classes = useStyles();
  const { children } = props;
  return (
    <Grid xs={12}>
      <Grid item xs={12}>
        <Box className={classes.root}>
          <Container maxWidth="md" className={classes.container}>
            <Paper className={classes.myPaper}>{children}</Paper>
          </Container>
        </Box>
      </Grid>
    </Grid>
  );
}
export default Wrapper;
