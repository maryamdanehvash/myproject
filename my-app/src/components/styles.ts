import { makeStyles } from "@mui/styles";
import { theme } from "../Theme/myTheme";

export const useStyles = makeStyles({
  root: {
    paddingTop: theme.spacing(2),
    [theme.breakpoints.down("sm")]: {},
  },
  container: {
    justifyContent: "center",
    alignItems: "center",
  },
  myPaper: {
    boederRadius: 15,
  },
});
